/*
	This is a part of y2g
	Loads representation of the system form .yaml files.

	Copyright(C) 2016 Jakub Turowski
	jakubturowski92@gmail.com

	This program is free software; you can redistribute it and / or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110 - 1301, USA.
*/

#pragma once
#include "Component.hpp"
#include "yaml-cpp/yaml.h"
#include <boost/filesystem.hpp>
#include <fstream>

class YAMLParser
{
public:
	// Structure containing entire loaded and parsed component system.
	class System {
		std::string name;
		std::string author;
		std::vector<std::shared_ptr<const Component>> components;
		std::vector<std::shared_ptr<CType>> types;
	public:
		const std::string& Name() const { return name; }
		const std::string& Author() const { return author; }
		const std::vector<std::shared_ptr<const Component>>& Components() const { return components; }
		const std::vector<std::shared_ptr<CType>>& Types() const { return types; };

		std::string& Name() { return name; }
		std::string& Author() { return author; }
		std::vector<std::shared_ptr<const Component>>& Components() { return components; }
		std::vector<std::shared_ptr<CType>>& Types() { return types; };
	};

	class RealComponentInstance
	{
		std::string comp_name;
		std::string name;
	public:
		RealComponentInstance(const YAML::Node &node)
		{
			comp_name = node["component"].as<std::string>();
			name = node["name"].as<std::string>();
		}
		std::string ComponentName() const { return comp_name; }
		std::string Name() const { return name; }
	};

private:
	static void ParseDir(const boost::filesystem::path&,
		std::vector<std::shared_ptr<const Component>>& abstr,
		std::vector<std::shared_ptr<const ComponentClass>>& real,
		std::vector<std::shared_ptr<const RealComponentInstance>>& inst);
	static void ParseTypes(System& syst, const boost::filesystem::path& dir, std::string name_base = "");
public:
	YAMLParser();
	~YAMLParser();
	static std::shared_ptr<System> ParseSystem(boost::filesystem::path systemfile);
};

