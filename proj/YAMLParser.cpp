/*
	This is a part of y2g
	Loads representation of the system form .yaml files.

	Copyright(C) 2016 Jakub Turowski
	jakubturowski92@gmail.com

	This program is free software; you can redistribute it and / or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110 - 1301, USA.
*/

#include "YAMLParser.hpp"
#include <iostream>

using namespace std;
using namespace boost::filesystem;

YAMLParser::YAMLParser()
{
}


YAMLParser::~YAMLParser()
{
}

// Load and parse all .yaml files from directory, and push them to the right vectors.
void YAMLParser::ParseDir(const path& p, 
	vector<shared_ptr<const Component>>& abstr,
	vector<shared_ptr<const ComponentClass>>& real,
	vector<shared_ptr<const RealComponentInstance>>& real_inst)
{
	if (is_directory(p))
	{
		directory_iterator end_itr; // default construction yields past-the-end
		cout << "  Parsing " << p << endl;
		for (directory_iterator it(p); it != end_itr; it++)
		{
			if (is_regular_file(it->path()) && it->path().extension() == ".yaml")
			{
				cout << "    " << it->path().filename() << "... ";
				string s = it->path().string();
				std::ifstream ifs(s);
				YAML::Node node = YAML::Load(ifs);
				string obj = node["object"].as<string>();
				if (obj == "real")
				{
					shared_ptr<ComponentClass> comp(new ComponentClass(node));
					real.push_back(comp);
				}
				else if (obj == "abstract") {
					shared_ptr<Component> comp(new AbstractComponent(node));
					abstr.push_back(comp);
				}
				else if (obj == "instance") {
					shared_ptr<RealComponentInstance> ins(new RealComponentInstance(node));
					real_inst.push_back(ins);
				}
				cout << "done" << endl;
			}
		}
	}
	else {
		cerr << "YAMLParser::ParseDir(): Error: " << p << " is not a driectory" << endl;
	}
}

// Parse Types. This can be called recursively.
void YAMLParser::ParseTypes(System& syst, const path& dir, string name_base)
{
	if (is_directory(dir))
	{
		directory_iterator end_itr; // default construction yields past-the-end
		cout << "  Parsing " << dir << endl;
		for (directory_iterator it(dir); it != end_itr; it++)
		{
			if (is_directory(it->path()))
			{
				ParseTypes(syst, it->path(), it->path().filename().string() + "::");
			}
			else if (is_regular_file(it->path()) && it->path().extension() == ".yaml")
			{
				std::ifstream ifs(it->path().string());
				YAML::Node node = YAML::Load(ifs);
				shared_ptr<CType> p(new CType(node, name_base));
				syst.Types().push_back(p);
			}
		}
	}
	else {
		cerr << "YAMLParser::ParseTypes(): Error: " << dir << " is not a driectory" << endl;
	}
}

shared_ptr<YAMLParser::System> YAMLParser::ParseSystem(path systemfile)
{
	std::ifstream ifs(systemfile.string());
	if (!ifs.is_open()) {
		cerr << "YAMLParser::ParseSystem(): Error: Cannot open " << systemfile << endl;
		return nullptr;
	}
	// Loaded system .yaml
	YAML::Node system = YAML::Load(ifs);
	path p = systemfile.parent_path();

	// Construct paths
	path abstract_path(p);
	path real_path(p);
	path instance_path(p);
	path types_path(p);
	vector<shared_ptr<const Component>> abstr;
	vector<shared_ptr<const ComponentClass>> real;
	vector<shared_ptr<const RealComponentInstance>> inst;

	abstract_path /= system["path"]["Abstract"].as<std::string>();
	real_path 	  /= system["path"]["Real"].as<std::string>();
	instance_path /= system["path"]["Instance"].as<std::string>();
	types_path    /= system["path"]["Types"].as<std::string>();

	// Structure containing entire loaded component system
	shared_ptr<System> syst(new System);
	syst->Name() = system["component"].as<std::string>();
	syst->Author() = system["author"].as<std::string>();
	
	// Load stuff from all directories
	ParseDir(abstract_path, abstr, real, inst);
	ParseDir(real_path, abstr, real, inst);
	ParseDir(instance_path, abstr, real, inst);
	ParseTypes(*syst, types_path);

	// Add all abstract components to the vector
	syst->Components() = abstr;

	// Real Component classes have been loaded,
	// now they must be bound to instances.
	// For each Real Component class
	for (auto it = real.begin(); it != real.end(); it++)
	{
		shared_ptr<const ComponentClass> comp = *it;
		// For each instance of a Real Component
		for (auto jt = inst.begin(); jt != inst.end(); jt++)
		{
			// Match them by name
			if (comp->Name() == (*jt)->ComponentName())
			{
				// Generate instance and add to the vector.
				string inst_name = (*jt)->Name();
				shared_ptr<const Component> real_inst(new RealComponent(comp, inst_name));
				syst->Components().push_back(real_inst);
			}
		}
	}
	
	return syst;
}
