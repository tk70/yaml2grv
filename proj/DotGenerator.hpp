/*
	This is a part of y2g
	Generates .dot files for the components of the system.

	Copyright(C) 2016 Jakub Turowski
	jakubturowski92@gmail.com

	This program is free software; you can redistribute it and / or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110 - 1301, USA.
*/

#pragma once
#include "Component.hpp"
#include "YAMLParser.hpp"
#include "HTMLWriter.hpp"
#include <boost/filesystem.hpp>
#include <fstream>
#include <string>
#include <vector>

class DotGenerator
{
public:
	struct Config
	{
		bool links;
		bool generate_svg;
		std::string rankdir;
		float ranksep;
		float nodesep;
		boost::filesystem::path out_path;
		boost::filesystem::path html_reference_path;
		Config():
			links(true),
			generate_svg(true),
			rankdir("LR"),
			ranksep(2.0f),
			nodesep(0.33f)
		{

		}
	};
	DotGenerator(const Config& cfg);
	~DotGenerator() {};
	int Generate(const YAMLParser::System& syst);

private:
	Config config;
	boost::filesystem::path html_path;

	int GenerateHTML(const YAMLParser::System& syst);
	int GenerateAbstract(const AbstractComponent& comp, const std::vector<std::shared_ptr<const Component>>& system_components);
};

