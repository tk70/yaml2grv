y2g
by Jakub Turowski
January 2016


Description
-----------
The program processes a YAML representation of a ROS/OROCOS component system
and generates documentation in form of HTML files and SVG graphs.
The graphs are generated using external "dot" command (Graphviz).

The program compiles on Windows on Linux. For Windows, a Visual Studio 2015
project has been included, for Linux a makefile has been provided.


Requirements
------------
Graphviz software - http://www.graphviz.org/
Note: "dot" binary must be added to PATH environment variable,
so the program can generate SVG files automatically.


Command line options
--------------------
-help          - Display command line options listed below.

-in <path>     - Specify .yaml system file or directory with .yaml system
                 files to parse. May be absolute or relative.
				 
-out <path>    - [optional] specify path for directory where files will be
                 generated. If not specified, a new folder will be created in
                 the YAML directory. May be absolute or relative.
				 
-g   <0/1>     - [optional] generate .svg files (default is 1).

-ranksep <val> - [optional] specify ranksep parameter for graphs.
                 (default is 2). See Graphviz documentation for more info.
				 
-nodesep <val> - [optional] specify nodesep parameter for graphs
                 (default is 0.33). See Graphviz documentation for more info.

				 
License
-------
Copyright(C) 2016 Jakub Turowski
jakubturowski92@gmail.com

This program is free software; you can redistribute it and / or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110 - 1301, USA.