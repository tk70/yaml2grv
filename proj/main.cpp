/*
	y2g - main file

	Copyright(C) 2016 JakubTurowski
	jakubturowski92@gmail.com

	This program is free software; you can redistribute it and / or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110 - 1301, USA.
*/

#include <iostream>
#include "CIO.hpp"
#include "Component.hpp"
#include "YAMLParser.hpp"
#include "DotGenerator.hpp"
#include <boost/filesystem/path.hpp>

using namespace std;
using namespace boost;

void parse(filesystem::path dir, DotGenerator::Config& cfg)
{
	cout << "Parsing system file: " << dir << endl;
	std::shared_ptr<YAMLParser::System> syst = YAMLParser::ParseSystem(dir);
	cout << "Generating .dot files" << endl;
	DotGenerator dot_gen(cfg);
	dot_gen.Generate(*syst);
}

int main(int argc, char**argv)
{
	// COnfig structure for DotGenerator
	DotGenerator::Config cfg;

	// Parse command line arguments
	filesystem::path yaml_dir;
	filesystem::path dot_dir;
	filesystem::path html_dir;
	for (int i = 1; i < argc; i++)
	{
		string arg(argv[i]);
		if (arg == "-help") {
			cout << "Commandline options:" << endl
				<< " -in  <path>     specify .yaml system file or directory with .yaml system files" << endl
				<< " -out <path>     (optional) specify directory for generated files" << endl
				<< " -g   <0/1>      (optional) generate .svg files (default " << (int)cfg.generate_svg << ")" << endl
				<< " -ranksep <val>  (optional) specify ranksep parameter for graphs (default " << cfg.ranksep << ")" << endl
				<< " -nodesep <val>  (optional) specify nodesep parameter for graphs (default " << cfg.nodesep << ")" << endl;
			return 0;
		}
		if (++i >= argc)
		{
			cerr << "Error: No parameter given for argument \"" << arg << "\"" << endl;
			return 1;
		}
		string param(argv[i]);
		if (arg == "-in") {
			yaml_dir = param;
		}
		else if (arg == "-out") {
			dot_dir = param;
		}
		else if (arg == "-g") {
			cfg.generate_svg = (param == "1");
		}
		else if (arg == "-ranksep") {
			cfg.ranksep = stof(param);
		}
		else if (arg == "-nodesep") {
			cfg.nodesep = stof(param);
		}
	}

	if (yaml_dir.empty()) {
		cerr << "Error: No input directory given, see -help" << endl;
		return 1;
	}

	// Construct input path
	yaml_dir = yaml_dir.make_preferred();
	if (!filesystem::exists(yaml_dir)) {
		cerr << "Error: Input file or directory (" << yaml_dir.string() << ") does not exist" << endl;
		return 1;
	}
	
	// Construct output path
	if (dot_dir.empty()) {
		// if the user haven't specified -out directory
		if (filesystem::is_directory(yaml_dir)) {
			dot_dir = yaml_dir;
		}
		else if (is_regular_file(yaml_dir))
		{
			dot_dir = yaml_dir.parent_path();
		}
		else
		{
			cerr << "main(): Error: " << yaml_dir << " is neither a file nor a directory" << endl;
			return 1;
		}
		dot_dir /= "dot";
		dot_dir = dot_dir.make_preferred();
	}
	
	if (!filesystem::exists(dot_dir)) {
		if (!filesystem::create_directory(dot_dir)) {
			cerr << "Error: Output directory (" << dot_dir.string()
			     << ") does not exist and could not be created" << endl;
			return 1;
		}
	}

	// Simplify the paths (remove ../s)
	yaml_dir = filesystem::canonical(yaml_dir);
	dot_dir = filesystem::canonical(dot_dir);

	// Output path for .dot files
	cfg.out_path = dot_dir;

	// Parse the .yaml file or the entire directory
	if (filesystem::is_directory(yaml_dir))
	{
		cout << "Parsing system file directory: " << yaml_dir << endl;
		filesystem::directory_iterator end_itr; // default construction yields past-the-end
		for (filesystem::directory_iterator it(yaml_dir); it != end_itr; it++)
		{
			if (it->path().extension() == ".yaml")
			{
				parse(it->path(), cfg);
			}
		}
	}
	else if (is_regular_file(yaml_dir))
	{
		parse(yaml_dir, cfg);
	}
		
	return 0;
}
