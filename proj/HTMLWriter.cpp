/*
	This is a part of y2g
	Produces HTML files for the system.

	Copyright(C) 2016 Jakub Turowski
	jakubturowski92@gmail.com

	This program is free software; you can redistribute it and / or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110 - 1301, USA.
*/

#include "HTMLWriter.hpp"
using namespace std;
using namespace boost::filesystem;

HTMLWriter::HTMLWriter() :
	reference_path("")
{
}

HTMLWriter::HTMLWriter(boost::filesystem::path reference_path):
	reference_path(reference_path)
{
}

HTMLWriter::~HTMLWriter()
{
}


string HTMLWriter::ToString() const
{
	std::stringstream s("");
	s << "<!DOCTYPE html>" << endl;
	s << "<html>" << endl;
	s << "<head>" << endl;
	s << "<title>" << title << "</title>" << endl;
	s << "</head>" << endl;
	s << "<body>" << endl;
	s << "<h1>" << name << "</h1>" << endl;
	s << "<h3>" << type << "</h3>" << endl;
	s << "<hr>" << endl;
	if (!class_.empty()) {
		s << "<h2>Class</h2>" << endl;
		s << "<p>" << class_ << "</p>" << endl;
	}
	if (!author.empty()) {
		s << "<h2>Author</h2>" << endl;
		s << "<p>" << author << "</p>" << endl;
	}
	if (!description.empty()) {
		s << "<h2>Description</h2>" << endl;
		s << "<p>" << description << "</p>" << endl;
	}
	if (!remarks.empty()) {
		s << "<h2>Remarks</h2>" << endl;
		s << "<p>" << remarks << "</p>" << endl;
	}
	if (!content.empty()) {
		s << "<hr>" << endl;
		s << content << endl;
	}
	if (!footnote.empty()) {
		s << "<hr>" << endl;
		s << footnote << endl;
	}
	s << "</body>" << endl;
	s << "</html>" << endl;
	return s.str();
}


int HTMLWriter::WriteFile(boost::filesystem::path dir) const
{
	std::ofstream ofs;
	dir = dir.make_preferred();
	if (!exists(dir)) {
		if (!create_directory(dir)) {
			cerr << "Error: Output directory (" << dir.string()
				<< ") does not exist and could not be created" << endl;
			return 1;
		}
	}
	ofs.open((dir / ((filename.empty()? SafeString(name) : filename) + ".html")).string());
	ofs << ToString();
	ofs.close();
	return 0;
}

string HTMLWriter::GenerateContent(const Component& comp) const
{
	std::stringstream s("");
	if (dynamic_cast<const AbstractComponent*>(&comp))
	{
		s << "<h3><a href=\"../svg/" << SafeString(comp.Name());
		s << ".svg\">[Graph]</a></h3>" << endl;
	}
	s << "<a name=\"inputs\"></a>" << endl;
	s << "<h2>Inputs</h2>" << endl;
	s << "<table border = \"1\">" << endl;
	s << " <tr>" << endl;
	s << "  <td><b>Name</b></td>" << endl;
	s << "  <td><b>Type</b></td>" << endl;
	s << "  <td><b>Description</b></td>" << endl;
	s << " </tr>" << endl;
	for (auto it = comp.Class().Inputs().Entries().begin(); it != comp.Class().Inputs().Entries().end(); it++) {
		s << " <tr>" << endl;
		s << "  <td>" << it->Name() << "</td>" << endl;
		s << "  <td>" << "<a href=\"types/" << SafeString(it->Type()) << ".html\">" << it->Type() << "</a></td>" << endl;
		s << "  <td>" << it->Description() << "</td>" << endl;
		s << " </tr>" << endl;
	}
	s << "</table>" << endl;

	s << "<a name=\"outputs\"></a>" << endl;
	s << "<h2>Outputs</h2>" << endl;
	s << "<table border = \"1\">" << endl;
	s << " <tr>" << endl;
	s << "  <td><b>Name</b></td>" << endl;
	s << "  <td><b>Type</b></td>" << endl;
	s << "  <td><b>Description</b></td>" << endl;
	s << " </tr>" << endl;
	for (auto it = comp.Class().Outputs().Entries().begin(); it != comp.Class().Outputs().Entries().end(); it++) {
		s << " <tr>" << endl;
		s << "  <td>" << it->Name() << "</td>" << endl;
		s << "  <td>" << "<a href=\"types/" << SafeString(it->Type()) << ".html\">" << it->Type() << "</a></td>" << endl;
		s << "  <td>" << it->Description() << "</td>" << endl;
		s << " </tr>" << endl;
	}
	s << "</table>" << endl;
	if (comp.Class().Parameters().Size() > 0) {
		s << "<a name=\"def_params\"></a>" << endl;
		s << "<h2>Default parameters</h2>" << endl;
		s << "<table border = \"1\">" << endl;
		s << " <tr>" << endl;
		s << "  <td><b>Name</b></td>" << endl;
		s << "  <td><b>Type</b></td>" << endl;
		s << "  <td><b>Value</b></td>" << endl;
		s << "  <td><b>Units</b></td>" << endl;
		s << "  <td><b>Description</b></td>" << endl;
		s << " </tr>" << endl;
		for (auto it = comp.Class().Parameters().Entries().begin(); it != comp.Class().Parameters().Entries().end(); it++) {
			s << " <tr>" << endl;
			s << "  <td>" << it->Name() << "</td>" << endl;
			s << "  <td>" << "<a href=\"types/" << SafeString(it->Type()) << ".html\">" << it->Type() << "</a></td>" << endl;
			s << "  <td>" << it->Value() << "</td>" << endl;
			s << "  <td>" << it->Units() << "</td>" << endl;
			s << "  <td>" << it->Description() << "</td>" << endl;
			s << " </tr>" << endl;
		}
		s << "</table>" << endl;
	}

	return s.str();
}

void HTMLWriter::Process(const Component& comp)
{
	name = comp.Name();
	title = comp.Name();
	class_ = comp.Class().Name() + ": <i>" + comp.Type() + "</i>";
	author = comp.Class().Author();
	description = comp.Class().Description();
	remarks = comp.Class().Remarks();
	content = GenerateContent(comp);
	footnote = StandardFootnote();
}

void HTMLWriter::Process(const Connection& con)
{
	filename = SafeString(con.Source().Component() + ":" + con.Source().Port() + " -> " + con.Target().Component() + ":" + con.Target().Port());
	name = "Connection";
	title = "Connection";
	std::stringstream s("");
	s << "<h3>";
	if (con.Source().Component() == "IN") {
		s << "Inputs";
	}
	else {
		s << "<a href=\"../" << SafeString(con.Source().Component());
		s << ".html\">" << con.Source().Component() << "</a>";
	}
	s << "::" << con.Source().Port();
	s << " -> ";
	if (con.Target().Component() == "OUT") {
		s << "Outputs";
	}
	else {
		s << "<a href=\"../" << SafeString(con.Target().Component());
		s << ".html\">" << con.Target().Component() << "</a>";
	}
	s << "::" << con.Target().Port();
	s << "</h3>";
	description = "<p>Connects two components</p>" + s.str();;
	footnote = StandardFootnote("../");
}

void HTMLWriter::Process(const CType& type)
{
	name = type.Name();
	title = type.Name();
	class_ = "Type";
	author = type.Author();
	std::stringstream s("");
	s << "<h2>Fields</h2>" << endl;
	s << "<table border = \"1\">" << endl;
	s << " <tr>" << endl;
	s << "  <td><b>Name</b></td>" << endl;
	s << "  <td><b>Type</b></td>" << endl;
	s << "  <td><b>Units</b></td>" << endl;
	s << "  <td><b>Description</b></td>" << endl;
	s << " </tr>" << endl;
	for (auto it = type.Entries().begin(); it != type.Entries().end(); it++) {
		s << " <tr>" << endl;
		s << "  <td>" << it->Name() << "</td>" << endl;
		s << "  <td>" << it->Type() << "</td>" << endl;
		s << "  <td>" << it->Units() << "</td>" << endl;
		s << "  <td>" << it->Description() << "</td>" << endl;
		s << " </tr>" << endl;
	}
	s << "</table>" << endl;
	content = s.str();
	footnote = StandardFootnote("../");
}

void HTMLWriter::Process(const YAMLParser::System& syst)
{
	name = syst.Name();
	filename = "index";
	title = syst.Name();
	class_ = "System";
	author = syst.Author();
	std::stringstream s("");
	s << "<h2>Components</h2>" << endl;
	s << "<table border = \"1\">" << endl;
	s << " <tr>" << endl;
	s << "  <td><b>Name</b></td>" << endl;
	s << "  <td><b>Graph</b></td>" << endl;
	s << "  <td><b>Type</b></td>" << endl;
	s << "  <td><b>Author</b></td>" << endl;
	s << "  <td><b>Description</b></td>" << endl;
	s << " </tr>" << endl;
	for (auto it = syst.Components().begin(); it != syst.Components().end(); it++) {
		s << " <tr>" << endl;
		s << "  <td><a href=\"" << SafeString((*it)->Name());
		s << ".html\">" << (*it)->Name() << "</a></td>" << endl;
		if (dynamic_cast<const AbstractComponent*>(it->get()))
		{
			s << "  <td><a href=\"../svg/" << SafeString((*it)->Name());
			s << ".svg\">[Link]</a></td>" << endl;
		}
		else {
			s << "  <td></td>" << endl;
		}
		s << "  <td>" << (*it)->Type() << "</td>" << endl;
		s << "  <td>" << (*it)->Class().Author() << "</td>" << endl;
		s << "  <td>" << (*it)->Class().Description() << "</td>" << endl;
		s << " </tr>" << endl;
	}
	s << "</table>" << endl;

	s << "<h2>Types</h2>" << endl;
	s << "<table border = \"1\">" << endl;
	s << " <tr>" << endl;
	s << "  <td><b>Name</b></td>" << endl;
	s << "  <td><b>Author</b></td>" << endl;
	s << " </tr>" << endl;
	for (auto it = syst.Types().begin(); it != syst.Types().end(); it++) {
		s << " <tr>" << endl;
		s << "  <td><a href=\"types/" << SafeString((*it)->Name());
		s << ".html\">" << (*it)->Name() << "</a></td>" << endl;
		s << "  <td>" << (*it)->Author() << "</td>" << endl;
		s << " </tr>" << endl;
	}
	s << "</table>" << endl;
	footnote = StandardFootnote();
	content = s.str();
}

std::string HTMLWriter::SafeString(const std::string& s)
{
	string t("");
	t.reserve(s.size());
	for (int i = 0; i < s.length(); i++) {
		char c = s[i];
		if (c >= '0' && c <= '9') t += c;
		else if (c >= 'a' && c <= 'z') t += c;
		else if (c >= 'A' && c <= 'Z') t += c;
		else if (c == '-' || c == '.') t += c;
		else t += '_';
	}
	return t;
}

std::string HTMLWriter::StandardFootnote(string url) const
{
	string s = "<h3><a href = \"" + url
		+ "index.html\">[Index]</a></h3><p>Generated with y2g</p>";
	return s;
}