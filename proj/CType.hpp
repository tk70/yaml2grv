/*
	This is a part of y2g
	Single type representation.

	Copyright(C) 2016 Jakub Turowski
	jakubturowski92@gmail.com

	This program is free software; you can redistribute it and / or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110 - 1301, USA.
*/

#pragma once
#include <string>
#include <vector>
#include "yaml-cpp/yaml.h"

class CType
{
public:
	class Entry
	{
		std::string type;
		std::string name;
		std::string description;
		std::string units;

	public:
		std::string& Type() { return type; }
		std::string& Name() { return name; }
		std::string& Description() { return description; }
		std::string& Units() { return units; }
		const std::string& Type() const { return type; }
		const std::string& Name() const { return name; }
		const std::string& Description() const { return description; }
		const std::string& Units() const { return units; }

		Entry(const YAML::Node& node)
		{
			name = node["name"].as<std::string>();
			description = node["description"].as<std::string>();
			type = node["type"].as<std::string>();
			units = node["units"].as<std::string>();
		}
	};

private:
	std::vector<Entry> field_vect;
	std::string name;
	std::string author;

	void Append(Entry& entry)
	{
		field_vect.push_back(entry);
	}

public:
	CType(const YAML::Node& node, std::string base_name = "")
	{
		name = base_name + node["name"].as<std::string>();
		author = node["author"].as<std::string>();
		YAML::Node fields = node["field"];
		for (int i = 0; ; i++) {
			try {
				Entry entry(fields[i]);
				field_vect.push_back(entry);
			}
			catch (...)
			{
				break;
			}
		}
	}

	std::string& Name() { return name; }
	std::string& Author() { return author; }
	const std::string& Name() const { return name; }
	const std::string& Author() const { return author; }

	const std::vector<Entry>& Entries() const { return field_vect; }

	size_t Size() const { return field_vect.size(); }
};