/*
	This is a part of y2g
	Generates .dot files for the components of the system.

	Copyright(C) 2016 Jakub Turowski
	jakubturowski92@gmail.com

	This program is free software; you can redistribute it and / or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110 - 1301, USA.
*/

#include "DotGenerator.hpp"
using namespace std;
using namespace boost::filesystem;

DotGenerator::DotGenerator(const Config& cfg):
	config(cfg)
{

}

class Block
{
	string name;
	string visible_name;
	string html;
	string color;
	vector<string> inputs;
	vector<string> outputs;
	const DotGenerator::Config& cfg;

	// IO to string for block
	string OToString(const vector<string>& v) const
	{
		string out("{");
		for (auto it = v.begin(); it != v.end(); it++)
		{
			out.append("<out_");
			out.append(*it);
			out.append(">");
			out.append(*it);
			if (it+1 != v.end()) {
				out.append("|");
			}
		}
		out.append("}");
		return out;
	}

	// IO to string for block
	string IToString(const vector<string>& v) const
	{
		string out("{");
		for (auto it = v.begin(); it != v.end(); it++)
		{
			out.append("<in_");
			out.append(*it);
			out.append(">");
			out.append(*it);
			if (it + 1 != v.end()) {
				out.append("|");
			}
		}
		out.append("}");
		return out;
	}

public:

	Block(string name, string visible_name, const DotGenerator::Config& cfg, string color = "", string html = ""):
		name(name), visible_name(visible_name), cfg(cfg), color(color), html(html)
	{
	}
	// each node represented as block
	string ToString() const {
		string out(name);
		out.append("\n\t[");
		if (color != "") {
			out.append("style=filled, fillcolor=" + color + ", ");
		}
		out.append("label=\"{");
		if (!inputs.empty()) {
			out.append(IToString(inputs));
			out.append("|");
		}

		out.append(visible_name);

		if (!outputs.empty()) {
			out.append("|");
			out.append(OToString(outputs));
		}
		out.append("}\"");
		if (cfg.links) {
			out.append(", URL=\"");
			string tmp = html.empty() ? HTMLWriter::SafeString(name) + ".html" : html;
			out.append(tmp);
			out.append("\", tooltip=\"");
			out.append(tmp);
			out.append("\"");
		}
		out.append("];");
		return out;
	}

	void AddI(std::string name)
	{
		inputs.push_back(name);
	}

	void AddO(std::string name)
	{
		outputs.push_back(name);
	}
};

int DotGenerator::GenerateAbstract(const AbstractComponent& comp, const vector<shared_ptr<const Component>>& system_components)
{
	std::ofstream ofs;
	cout << "Generating " << comp.Name() << ".dot" << endl;
	path write_path = config.out_path / comp.Name();
	ofs.open(write_path.string() + ".dot");

	// Header
	ofs << "digraph " << comp.Name() << " {" << endl
		<< "graph [rankdir = " << config.rankdir
		<< ", ranksep=" << config.ranksep
		<< ", nodesep=" << config.nodesep
		<< "];" << endl
		<< "node[shape=record];" << endl;

	// Generate input blocks
	ofs << endl;
	ofs << "/* IO */" << endl;
	for (auto it = comp.Class().Inputs().Entries().begin(); it != comp.Class().Inputs().Entries().end(); it++)
	{
		Block inputs(
			"in_" + it->Name(),
			it->Name(),
			config,
			"lightskyblue",
			"../html/" + HTMLWriter::SafeString(comp.Name()) + ".html#inputs"
		);

		ofs << inputs.ToString() << endl;
	}

	// Generate output blocks
	for (auto it = comp.Class().Outputs().Entries().begin(); it != comp.Class().Outputs().Entries().end(); it++)
	{
		Block outputs(
			"out_" + it->Name(),
			it->Name(),
			config,
			"lightgoldenrod1",
			"../html/" + HTMLWriter::SafeString(comp.Name()) + ".html#outputs"
		);
		ofs << outputs.ToString() << endl;
	}

	// Generate components
	ofs << endl;
	ofs << "/* Nested components */" << endl;
	const vector<string>& children = comp.Children();
	for (auto it = children.begin(); it != children.end(); it++)
	{
		string child_name = *it;
		for (auto jt = system_components.begin(); jt != system_components.end(); jt++)
		{
			const Component& child = **jt;
			if (child.Name() == child_name) {

				// Create the component
				Block* child_block = nullptr;
				if (dynamic_cast<const AbstractComponent*>(&child))
				{
					child_block = new Block(child_name, child_name, config, "bisque",
						HTMLWriter::SafeString(child_name) + ".svg"
					);
				}
				else {
					child_block = new Block(child_name, child_name, config, "lightyellow",
						"../html/" + HTMLWriter::SafeString(child_name) + ".html"
					);
				}

				// Append IOs to component
				for (auto kt = child.Class().Inputs().Entries().begin();
				kt != child.Class().Inputs().Entries().end(); kt++)
				{
					child_block->AddI(kt->Name());
				}
				for (auto kt = child.Class().Outputs().Entries().begin();
				kt != child.Class().Outputs().Entries().end(); kt++)
				{
					child_block->AddO(kt->Name());
				}
				ofs << child_block->ToString() << endl;
				delete child_block;
			}
		}
	}
	ofs << endl;

	ofs << "/* Connections */" << endl;
	const vector<Connection>& connections = comp.Connections();
	for (auto it = connections.begin(); it != connections.end(); it++)
	{
		string label("");
		string filename("");
		const Connection& con = *it;
		if (con.Source().Component() == "IN") {
			label.append("in_" + con.Source().Port());
		}
		else {
			label.append(con.Source().Component() + ":out_" + con.Source().Port());
		}
		filename.append(con.Source().Component() + ":" + con.Source().Port());
		label.append(" -> ");
		filename.append(" -> ");
		if (con.Target().Component() == "OUT") {
			label.append("out_" + con.Target().Port());
		}
		else {
			label.append(con.Target().Component() + ":in_" + con.Target().Port());
		}
		filename.append(con.Target().Component() + ":" + con.Target().Port());
		ofs << label;
		ofs << endl;
		ofs << "\t[URL=\"../html/connections/" << HTMLWriter::SafeString(filename) << ".html\", tooltip=\"";
		ofs << label << "\"]";
		ofs << endl;
	}

	ofs << "}";
	ofs.close();
	return 0;
}

int DotGenerator::GenerateHTML(const YAMLParser::System& syst)
{
	// Path for HTML files of the system
	html_path = config.out_path / "html";

	// Create the directory if it doesn't exist
	if (!boost::filesystem::exists(html_path)) {
		if (!create_directory(html_path)) {
			cerr << "Error: Output directory (" << html_path.string()
				<< ") does not exist and could not be created" << endl;
			return 1;
		}
	}

	cout << "Generating HTML files... ";

	HTMLWriter hw(config.html_reference_path);
	hw.Process(syst);
	hw.WriteFile(html_path);

	// Path for connection HTMLs
	path connections_path = html_path / "connections";

	// Iterate through all components 
	for (auto it = syst.Components().begin(); it != syst.Components().end(); it++)
	{
		// Generate HTML for the component
		HTMLWriter hw(config.html_reference_path);
		hw.Process(**it);
		hw.WriteFile(html_path);

		// If the component is Abstract, generate connection HTMLs for it
		if (const AbstractComponent* cp = dynamic_cast<const AbstractComponent*>(it->get())) {
			const vector<Connection>& connections = cp->Connections();
			for (auto jt = connections.begin(); jt != connections.end(); jt++)
			{
				HTMLWriter hw(config.html_reference_path);
				hw.Process(*jt);
				hw.WriteFile(connections_path);
			}
		}
	}

	// Path for Type HTMLs
	path types_path = html_path / "types";

	// Iterate through all types
	for (auto it = syst.Types().begin(); it != syst.Types().end(); it++)
	{
		HTMLWriter hw(config.html_reference_path);
		hw.Process(**it);
		hw.WriteFile(types_path);
	}

	cout << "done" << endl;
	return 0;
}

int DotGenerator::Generate(const YAMLParser::System& syst)
{
	if (GenerateHTML(syst)) return 1;
	
	std::ofstream script;

	// Svg folder path
	path svg_dir = config.out_path / "svg";

	// Generate batch/bash script for Graphviz, depending on current platform
	// Path of the script
#ifdef _WIN32
	path script_write_path = config.out_path / "svg_generator.bat";
#elif __linux__
	path script_write_path = config.out_path / "svg_generator.sh";
#endif

	script.open(script_write_path.string());

#ifdef _WIN32

#elif __linux__
	script << "#!/bin/bash" << endl;
#endif

	// Iterate through all components 
	for (auto it = syst.Components().begin(); it != syst.Components().end(); it++)
	{
		// If the component is Abstract, generate .dot file for it.
		if (const AbstractComponent* cp = dynamic_cast<const AbstractComponent*>(it->get())) {
			GenerateAbstract(*cp, syst.Components());
			path comp_path = config.out_path / cp->Name();
#ifdef _WIN32
			script << "CALL \"dot.exe\" ";
#elif __linux__
			script << "dot ";
#endif
			script << "-Tsvg \""
				<< (config.out_path / cp->Name()).string() << ".dot\" -o \""
				<< (svg_dir / cp->Name()).string() << ".svg\"" << endl;
		}
	}
	script.close();

	// Execute the script to generate .svg files
	if (config.generate_svg) {

		// Create the folder if it doesn't exist
		if (!boost::filesystem::exists(svg_dir)) {
			if (!create_directory(svg_dir)) {
				cerr << "Error: Output directory (" << svg_dir.string()
					<< ") does not exist and could not be created" << endl;
				return 1;
			}
		}

		cout << "Generating .svg files..." << endl;
#ifdef _WIN32
		system(script_write_path.string().c_str());
#elif __linux__
		system((string("chmod u+x ") + script_write_path.string()).c_str());
		system((string("/bin/bash -c ") + script_write_path.string()).c_str());
#endif
		cout << "...done." << endl;
	}
	return 0;
}
