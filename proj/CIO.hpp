/*
	This is a part of y2g
	Inputs and outputs of a component.

	Copyright(C) 2016 Jakub Turowski
	jakubturowski92@gmail.com

	This program is free software; you can redistribute it and / or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110 - 1301, USA.
*/

#pragma once

#include <string>
#include <vector>
#include <memory>
#include "CType.hpp"
#include "yaml-cpp/yaml.h"

class CIO
{
public:
	//enum SRT { S, RT };
private:
	std::string name;
	std::string description;
	std::string type;
	//SRT srt;
public:

	const std::string& Name() const { return name; }
	const std::string& Type() const { return type; }
	const std::string& Description() const { return description; }
	//SRT Srt() const { return srt; }

	CIO(const YAML::Node &node)
	{
		name = node["name"].as<std::string>();
		type = node["type"].as<std::string>();
		description = node["description"].as<std::string>();
	}
};

class CInputs
{
public:
	class CInput: public CIO
	{
	public:
		CInput(const YAML::Node &node): CIO(node) {}
	};

private:
	std::vector<CInput> i_struct_v;

public:

	CInputs(const YAML::Node &node)
	{
		for (int i = 0; ; i++) {
			try {
				YAML::Node n = node[i];
				CInput ci = CInput(n);
				Append(ci);
			}
			catch (...)
			{
				break;
			}
		}
	}

	CInput& operator [] (int index)
	{
		return i_struct_v[index];
	}

	const std::vector<CInput>& Entries() const { return i_struct_v; }

	size_t Size() const
	{
		return i_struct_v.size();
	}

	size_t Append(CInput& ci)
	{
		i_struct_v.push_back(ci);
		return i_struct_v.size();
	}
};

class COutputs
{
public:
	class COutput: public CIO
	{
		std::string Activity;
	public:
		COutput(const YAML::Node &node) :
			CIO(node)
		{
		}
	};

private:
	std::vector<COutput> i_struct_v;

public:

	COutputs(const YAML::Node &node)
	{
		for (int i = 0; ; i++) {
			try {
				YAML::Node n = node[i];
				COutput co = COutput(n);
				Append(co);
			}
			catch (...)
			{
				break;
			}
		}
	}

	COutput& operator [] (int index)
	{
		return i_struct_v[index];
	}

	const std::vector<COutput>& Entries() const { return i_struct_v; }

	size_t Size() const
	{
		return i_struct_v.size();
	}

	size_t Append(COutput& co)
	{
		i_struct_v.push_back(co);
		return i_struct_v.size();
	}
};
