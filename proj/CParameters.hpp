/*
	This is a part of y2g
	Parameters of a component.
	
	Copyright(C) 2016 Jakub Turowski
	jakubturowski92@gmail.com

	This program is free software; you can redistribute it and / or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110 - 1301, USA.
*/

#pragma once
#include <vector>
#include <string>
#include "yaml-cpp/yaml.h"

// Class for component parameters
class CParameters
{
public:
	class Entry
	{
		std::string type;
		std::string name;
		std::string description;
		std::string units;
		std::string value;

	public:
		std::string& Type() { return type; }
		std::string& Name() { return name; }
		std::string& Description() { return description; }
		std::string& Units() { return units; }
		std::string& Value() { return value; }
		const std::string& Type() const { return type; }
		const std::string& Name() const { return name; }
		const std::string& Description() const { return description; }
		const std::string& Units() const { return units; }
		const std::string& Value() const { return value; }

		Entry(const YAML::Node& node)
		{
			name = node["name"].as<std::string>();
			description = node["description"].as<std::string>();
			type = node["type"].as<std::string>();
			units = node["units"].as<std::string>();
			value = node["value"].as<std::string>();
		}
	};

private:
	std::vector<Entry> par_vect;

public:
	size_t Size() const
	{
		return par_vect.size();
	}

	const std::vector<Entry>& Entries() const { return par_vect; }

	CParameters()
	{}

	CParameters(const YAML::Node &node)
	{
		for (int i = 0; ; i++) {
			try {
				Entry entry(node[i]);
				par_vect.push_back(entry);
			}
			catch (...)
			{
				break;
			}
		}
	}


};
