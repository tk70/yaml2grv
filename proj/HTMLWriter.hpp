/*
	This is a part of y2g
	Produces HTML files for the system.

	Copyright(C) 2016 Jakub Turowski
	jakubturowski92@gmail.com

	This program is free software; you can redistribute it and / or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110 - 1301, USA.
*/

#pragma once
#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>
#include <boost/filesystem.hpp>
#include "Component.hpp"
#include "YAMLParser.hpp"

class HTMLWriter
{
	std::string type;
	std::string name;
	std::string class_;
	std::string description;
	std::string remarks;
	std::string title;
	std::string author;
	std::string content;
	std::string footnote;
	std::string filename;
	boost::filesystem::path reference_path;

	std::string GenerateContent(const Component& comp) const;
	std::string StandardFootnote(std::string url = "") const;
public:
	HTMLWriter();
	HTMLWriter(boost::filesystem::path reference_path);
	~HTMLWriter();
	std::string& Type() { return type; }
	std::string& Name() { return name; }
	std::string& Class() { return class_; }
	std::string& Description() { return description; }
	std::string& Remarks() { return remarks; }
	std::string& Title() { return title; }
	std::string& Author() { return author; }
	std::string& Content() { return content; }
	std::string& Footnote() { return footnote; }
	std::string& Filename() { return filename; }

	const std::string& Type() const { return type; }
	const std::string& Name() const { return name; }
	const std::string& Class() const { return class_; }
	const std::string& Description() const { return description; }
	const std::string& Remarks() const { return remarks; }
	const std::string& Title() const { return title; }
	const std::string& Author() const { return author; }
	const std::string& Content() const { return content; }
	const std::string& Footnote() const { return footnote; }
	const std::string& Filename() const { return filename; }

	std::string ToString() const;
	int WriteFile(boost::filesystem::path path) const;
	void Process(const Component& comp);
	void Process(const Connection& con);
	void Process(const CType& type);
	void Process(const YAMLParser::System& syst);

	static std::string SafeString(const std::string& s);
};