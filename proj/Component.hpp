/*
	This is a part of y2g
	Component classes

	Copyright(C) 2016 Jakub Turowski
	jakubturowski92@gmail.com

	This program is free software; you can redistribute it and / or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110 - 1301, USA.
*/

#pragma once
#include "CIO.hpp"
#include "CParameters.hpp"
#include "yaml-cpp/yaml.h"
#include <memory>
#include <iostream>

// Connection type
// Connects two components in Abstract Component
class Connection
{
public:
	class Endpoint {
		std::string component;
		std::string port;
	public:
		std::string& Component() { return component; }
		std::string& Port() { return port; }
		const std::string& Component() const { return component; }
		const std::string& Port() const { return port; }
		Endpoint(std::string component, std::string port) : component(component), port(port) {}
		Endpoint() {}
	};
private:
	Endpoint source;
	Endpoint target;
public:
	Connection(Endpoint& source, Endpoint& target) : source(source), target(target) {}
	Connection() {}
	const Endpoint& Source() const { return source; }
	const Endpoint& Target() const { return target; }
};

// Component class of real/abstract component
// Hold basic information about the component, it's children, IO and default parameters
class ComponentClass
{
private:
	std::string name;
	std::string behaviour;
	std::string activity;
	std::string description;
	std::string remarks;
	std::string author;
	CInputs ci;
	COutputs co;
	CParameters parameters;

public:
	ComponentClass(const YAML::Node &node):
		ci(node["inputs"]), co(node["outputs"]), parameters(node["parameters"])
	{
		name = node["name"].as<std::string>();
		author = node["author"].as<std::string>();
		description = node["description"].as<std::string>();
	}
	//std::vector<std::string> Children() const { return children; }
	//std::string& Name() { return name; }
	//std::string& Activity() { return activity; }
	//std::string& Description() { return description; }
	//std::string& Remarks() { return remarks; }
	const std::string& Name() const { return name; }
	const std::string& Activity() const { return activity; }
	const std::string& Description() const { return description; }
	const std::string& Author() const { return author; }
	const std::string& Remarks() const { return remarks; }
	const CInputs& Inputs() const { return ci; }
	const COutputs& Outputs() const  { return co; }
	const CParameters& Parameters() const { return parameters; }
};

// Base instance of Component Class
class Component
{
protected:
	std::shared_ptr<const ComponentClass> cclass;
public:
	Component(std::shared_ptr<const ComponentClass> cclass):
		cclass(cclass)
	{
	}
	const ComponentClass& Class() const { return *cclass; }
	virtual const std::string& Name() const = 0;
	virtual const std::string Type() const = 0;
	virtual ~Component() {}
};

// Abstract component
class AbstractComponent: public Component
{
	std::vector<Connection> con_vect;
	std::vector<std::string> children;

	void LoadConnections(const YAML::Node &node)
	{
		for (int i = 0; ; i++) {
			try {
				Connection::Endpoint source, target;
				YAML::Node entry = node[i];
				source.Component() = entry["source"][0]["name"].as<std::string>();
				source.Port() = entry["source"][0]["output"].as<std::string>();

				for (int j = 0; ; j++)
				{
					try {
						target.Component() = entry["target"][j]["name"].as<std::string>();
						target.Port() = entry["target"][j]["input"].as<std::string>();
					}
					catch (...)
					{
						break;
					}
					con_vect.push_back(Connection(source, target));
				}
			}
			catch (...) { break; }
		}
	}

	void LoadMappings(const YAML::Node &node)
	{
		for (int i = 0; ; i++) {
			try {
				
				YAML::Node entry = node[i];
				std::string type = entry["type"].as<std::string>();
				if (type == "input") {
					Connection::Endpoint source, target;
					source.Component() = "IN";
					source.Port() = entry["port"].as<std::string>();
					for (int j = 0; ; j++) {
						YAML::Node tar_entry = entry["to"][j];
						try {
							target.Component() = tar_entry["name"].as<std::string>();
							target.Port() = tar_entry["port"].as<std::string>();
						}
						catch (...) { break; }
						con_vect.push_back(Connection(source, target));
					}
				}
				else if (type == "output") {
					Connection::Endpoint source, target;
					target.Component() = "OUT";
					target.Port() = entry["port"].as<std::string>();
					for (int j = 0; ; j++) {
						YAML::Node src_entry = entry["to"][j];
						try {
							source.Component() = src_entry["name"].as<std::string>();
							source.Port() = src_entry["port"].as<std::string>();
						}
						catch (...) { break; }
						con_vect.push_back(Connection(source, target));
					}
				}
			}
			catch (...)
			{
				break;
			}
		}
	}

	void LoadChildren(const YAML::Node &node)
	{
		try {
			for (int i = 0; ; i++) {
				try {
					children.push_back(node[i].as<std::string>());
				}
				catch (...)
				{
					break;
				}
			}
		}
		catch (...) {}
	}

public:
	AbstractComponent(const YAML::Node &node) :
		Component(std::shared_ptr<ComponentClass> (new ComponentClass(node)))
	{
		try {
			LoadConnections(node["connect"]);
		}
		catch (...) {
			std::cerr << "Failed to load connections for Abstract Component " << cclass->Name() << std::endl;
		}
		try {
			LoadMappings(node["map"]);
		}
		catch (...) {
			std::cerr << "Failed to load mappings for Abstract Component " << cclass->Name() << std::endl;
		}
		try {
			LoadChildren(node["children"]);
		}
		catch (...) {
			std::cerr << "Failed to load children for Abstract Component " << cclass->Name() << std::endl;
		}
	}

	virtual const std::string& Name() const { return cclass->Name(); }
	virtual const std::string Type() const { return "Abstract"; }
	const std::vector<Connection>& Connections() const { return con_vect; }
	const std::vector<std::string>& Children() const { return children; }
	virtual ~AbstractComponent() {}
};

// Real Component
// Instance of real component
class RealComponent: public Component
{
	CParameters params;		// custom parameters
	std::string name;
public:
	RealComponent(std::shared_ptr<const ComponentClass> cclass, std::string instance_name, const CParameters& params) :
		Component(cclass), name(instance_name), params(params)
	{
	}

	RealComponent(std::shared_ptr<const ComponentClass> cclass, std::string instance_name) :
		Component(cclass), name(instance_name)
	{
	}

	virtual const std::string& Name() const { return name; }
	virtual const std::string Type() const { return "Real"; }

	virtual ~RealComponent() {};
};

